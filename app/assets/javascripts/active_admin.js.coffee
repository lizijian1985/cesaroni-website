#= require active_admin/base
#= require jquery-are-you-sure

# For products, only allow one image to be primary image
window.trigger_image_primary = (self) ->
  $(".has_many_fields ol li label :checkbox").each((i, element) ->
    return if self == element or element.id.indexOf('primary') == -1
    $(element).prop('checked', false)
    return
  )
  return


$(document).ready () ->
  controller = $('body').data('controller')
  action = $('body').data('action')

  # Add "dirty forms" plugin to any forms. Warns user if content entered but not submitted.
  if action is 'new' or action is 'create' or action is 'edit'
    $('form').areYouSure();

  # Rename "Award Receiveds" to "Awards Received" in header
  $('#header').find('#award_receiveds').find('a').text('Awards Recieved')

  # On index views, rename action text
  if action is 'index'
    $('.table_actions').find('a').each (index, link) ->
      linkInstance = $(link)
      switch linkInstance.html()
        when 'Delete' then linkInstance.html('Remove')

  # Replace "Create #{resource}" with "Done"
  if action is 'new' or action is 'create'
    $('input[type=submit]').val('Done')

  # Replace "Cprocess" with "Process"
  if controller is 'processes' and action is 'edit'
    $('#cprocess_submit_action').find('input').val('Update Process')

  # Replace "Award" with "Award Recieved"
  if controller is 'award_receiveds' and action is 'edit'
    $('#award_submit_action').find('input').val('Update Award Recieved')



