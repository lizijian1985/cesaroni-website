class Product < ActiveRecord::Base
  belongs_to :industry
  has_many :awards
  has_many :images

  accepts_nested_attributes_for :images, allow_destroy: true

  mount_uploader :brochure, BrochureUploader

  validates_presence_of :name, :client, :date_completed, :industry, :description
  validate :validate_images

  def validate_images
    errors.add(:images, 'must be at least 1 image per Product') if self.images.length == 0
  end
end
