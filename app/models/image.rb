class Image < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  before_save :assign_name

  def assign_name
    t = self.image.file.path
    self.name = File.basename(t, File.extname(t)) unless self.name.present?
  end
end
