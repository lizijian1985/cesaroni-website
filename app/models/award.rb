class Award < ActiveRecord::Base
  belongs_to :product
  mount_uploader :image, ImageUploader

  validates_presence_of :name, :committee, :date_received, :image, :product
end
