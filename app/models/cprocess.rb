class Cprocess < ActiveRecord::Base
  mount_uploader :image, ImageUploader
  enum status: %w(1 2 3 4 5 6 7 8 9 10)
  validates_presence_of :title, :description, :phase, :order, :image
end
