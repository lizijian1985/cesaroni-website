class Industry < ActiveRecord::Base
  validates_presence_of :name

  has_many :products
  belongs_to :featured_product, :class_name => 'Product', :foreign_key => 'featured_product_id'
end
