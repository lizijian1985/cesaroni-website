ActiveAdmin.register Industry do
  permit_params :name, :featured_product_id

  index do
    selectable_column
    id_column
    column :name
    column :featured_product
    column :updated_at
    column :created_at
    actions
  end

  filter :name

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Industry Details" do
      f.input :name
      f.input :featured_product_id, as: :select, include_blank: false, collection: f.object.products
    end
    f.actions
  end
end
