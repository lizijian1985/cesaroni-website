ActiveAdmin.register Cprocess, as: 'Process' do
  permit_params :title, :image, :description, :phase, :order

  index do
    selectable_column
    id_column
    column :title
    column :description
    column :phase
    column :order
    column :updated_at
    column :created_at
    actions
  end

  filter :title
  filter :description
  filter :phase
  filter :order

  show do
    attributes_table do
      row :title
      row :description
      row :phase
      row :order
      row :image do |process|
        if process.image then image_tag(process.image.url) else 'None' end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs 'Product Details' do
      f.input :title
      f.input :description
      f.input :phase, :as => :select, :collection => ['Phase One', 'Phase Two', 'Phase Three']
      f.input :order, :as => :select, :collection => %w(1 2 3 4 5 6 7 8 9 10)
      f.input :image, as: :file, hint: if f.object.image.present? then image_tag(f.object.image.url) end
    end
    f.actions
  end
end

