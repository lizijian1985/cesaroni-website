ActiveAdmin.register Product do
  permit_params :brochure, :client, :description, :design_team, :name, :date_completed, :industry_id, images_attributes: [:id, :name, :image, :primary, :_destroy]

  index do
    selectable_column
    id_column
    column :client
    column :name
    column :date_completed
    column :design_team
    column :industry
    column :brochure do |product|
      if product.brochure then link_to('View Brochure', product.brochure.url) else 'None' end
    end
    column '# Images' do |product|
      product.images.count
    end
    column :updated_at
    column :created_at
    actions
  end

  show do
    attributes_table do
      row :client
      row :name
      row :date_completed
      row :design_team
      row :industry
      row :description
      row :brochure do |product|
        if product.brochure then link_to('View Brochure', product.brochure.url) else 'None' end
      end
      row 'Images' do
        product.images.each do |img|
          div :class => 'is-primary-image' do
            "*** Primary Image ***" if img.primary
          end
          div do
            image_tag(img.image.url)
          end
        end
      end
      row :updated_at
      row :created_at
    end
  end

  filter :name
  filter :client
  filter :date_completed
  filter :design_team
  filter :industry

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Product Details" do
      f.input :name
      f.input :client, :as => :datalist, :collection => Product.group(:client).count.keys
      f.input :date_completed, as: :datepicker
      f.input :design_team
      f.input :industry
      f.input :description
      f.input :brochure, as: :file
    end
    f.has_many :images do |ff|
      ff.input :primary, :input_html => { :onclick => 'trigger_image_primary(this)' }
      ff.input :name
      ff.input :image, as: :file, hint: if ff.object.image.present? then image_tag(ff.object.image.url) end
      ff.input :_destroy, :as => :boolean
    end
    f.actions
  end

  controller do
    def build_resource_params
      if params['product'].nil?
        super
      else
        product = super.first
        images = product['images_attributes']
        unless images.present? and images['0'] && images.any? { |(_, img)| img['primary'] == '1' }
           product['images_attributes']['0']['primary'] = '1' if product['images_attributes'].try(:[], '0').present?
        end
        # puts product.inspect
        [product]
      end
    end
  end
end

