ActiveAdmin.register Award, as: 'Award Received' do
  permit_params :committee, :image, :name, :date_received, :product_id

  index do
    selectable_column
    id_column
    column :name
    column :committee
    column :date_received
    column :product
    column :updated_at
    column :created_at
    actions
  end

  filter :name
  filter :committee
  filter :date_received
  filter :product

  show do
    attributes_table do
      row :name
      row :committee
      row :date_received
      row :product
      row :image do |award|
        if award.image then image_tag(award.image.url) else 'None' end
      end
    end
  end

  form do |f|
    f.semantic_errors *f.object.errors.keys
    f.inputs "Award Details" do
      f.input :name
      f.input :committee, :as => :datalist, :collection => Award.group(:committee).count.keys
      f.input :date_received, as: :datepicker
      f.input :product
      f.input :image, as: :file, hint: if f.object.image.present? then image_tag(f.object.image.url) end
    end
    f.actions
  end
end

