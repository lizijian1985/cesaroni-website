FROM rails:4.2

RUN mkdir -p /app/uploads

WORKDIR /app

# Copy the Gemfile as well as the Gemfile.lock and install
# the RubyGems. This is a separate step so the dependencies
# will be cached unless changes to one of those two files
# are made.
COPY Gemfile Gemfile.lock ./ 
RUN gem install bundler && bundle install --jobs 20 --retry 5 --without development test

# Set Rails to run in production
ENV RAILS_ENV production 
ENV RACK_ENV production

# Copy the main application.
COPY . ./

#RUN RAILS_ENV=production bundle exec rake assets:precompile --trace
VOLUME - ./public:/app/public

# Start puma
CMD bundle exec puma -C config/puma.rb
