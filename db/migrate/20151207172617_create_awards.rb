class CreateAwards < ActiveRecord::Migration
  def change
    create_table :awards do |t|
      t.string :name, null: false
      t.string :committee, null: false
      t.integer :year_awarded, null: false
      t.string :image, null: false

      t.references :product

      t.timestamps null: false
    end
  end
end
