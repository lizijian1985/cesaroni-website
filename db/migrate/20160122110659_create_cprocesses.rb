class CreateCprocesses < ActiveRecord::Migration
  def change
    create_table :cprocesses do |t|
      t.string :title
      t.text :description
      t.string :phase
      t.integer :order, default: 1
      t.string :image, null: false

      t.timestamps null: false
    end
  end
end
