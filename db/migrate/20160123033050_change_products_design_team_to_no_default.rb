class ChangeProductsDesignTeamToNoDefault < ActiveRecord::Migration
  def change
    change_column_default(:products, :design_team, nil)
  end
end
