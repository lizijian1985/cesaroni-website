class ChangeAwardYearAwardedToDate < ActiveRecord::Migration
  def change
    # remove not null constrain, since it will not be set in the future and will be null
    change_column_null(:awards, :year_awarded, true)
    # store historical data of year_awarded to year_awarded_old
    rename_column :awards, :year_awarded, :year_awarded_old
    # create new year_awarded with data type
    add_column :awards, :year_awarded, :date
  end
end
