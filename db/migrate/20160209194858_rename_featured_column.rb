class RenameFeaturedColumn < ActiveRecord::Migration
  def change
    rename_column :industries, :featured_id, :featured_product_id
  end
end
