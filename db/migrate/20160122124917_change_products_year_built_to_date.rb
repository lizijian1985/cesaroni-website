class ChangeProductsYearBuiltToDate < ActiveRecord::Migration
  def change
    rename_column :products, :year_built, :year_built_old
    add_column :products, :year_built, :date
  end
end
