class RenameYearToDateColumns < ActiveRecord::Migration
  def change
    rename_column :products, :year_built, :date_completed
    rename_column :awards, :year_awarded, :date_received
  end
end
