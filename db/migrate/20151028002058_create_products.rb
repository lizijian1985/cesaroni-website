class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :name, null: false
      t.string :client, null: false
      t.string :design_team, default: 'Cesaroni Design'
      t.text :description
      t.integer :year_built
      t.string :brochure

      t.references :industry

      t.timestamps null: false
    end

    create_table :images do |t|
      t.string :image, null: false
      t.string :name
      t.boolean :primary, default: false

      t.references :product

      t.timestamps null: false
    end
  end
end
