class CreateIndustries < ActiveRecord::Migration
  def change
    create_table :industries do |t|
      t.string :name, null: false
      t.integer :featured_id, null: true

      t.timestamps null: false
    end
  end
end
