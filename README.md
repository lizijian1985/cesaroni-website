# Cesaroni Design

## Requirements

- Ruby 2.2
- Bundler
- ImageMagick

## Development

1) Clone project:
```shell
git clone git@bitbucket.org:ptannenbaum/cesaroni-website.git
```

2) Install dependencies:
```shell
$ cd cesaroni
$ bundler install
```

3) Setup database and seeds:
```shell
$ rake db:create db:migrate db:seed
```

4) Run application:
```shell
$ bin/rails s
```

5) Visit the app:
[http://localhost:3000/admin](http://localhost:3000/admin)

## Production

1) SSH into the server:
```shell
$ ssh root@107.170.24.171
```

2) Move to app directory:
```shell
$ cd cesaroni
```

3) Pull the latest code:
```shell
$ git fetch --all
$ git pull origin master
```

4) Build new docker image:
```shell
$ docker-compose build web
```

5) Run migrations (_optional_):
```shell
$ docker-compose run web rake db:migrate
```
7) Update assets (_optional_):
```shell
$ rm -Rf ./public/assets # remove precompiled assets
$ RAILS_ENV=production rake assets:precompile # recompile assets in host env
```

6) Start new web image:
```shell
$ docker-compose stop web
$ docker-compose up -d web
```
